package Survivors.demo.config;

/**
 * Application constants.
 */
public final class Constants {
    public static final int SUCCESS = 1;
    public static final int ERROR = 0;
}
