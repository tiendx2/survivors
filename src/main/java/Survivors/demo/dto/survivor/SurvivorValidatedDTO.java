package Survivors.demo.dto.survivor;

import Survivors.demo.domain.Survivor;

public class SurvivorValidatedDTO {
    private Survivor survivor;
    private Integer codeResponse;
    private String messResponse;

    public SurvivorValidatedDTO(Survivor survivor, Integer codeResponse, String messResponse) {
        this.survivor = survivor;
        this.codeResponse = codeResponse;
        this.messResponse = messResponse;
    }

    public Survivor getSurvivor() {
        return survivor;
    }

    public void setSurvivor(Survivor survivor) {
        this.survivor = survivor;
    }

    public Integer getCodeResponse() {
        return codeResponse;
    }

    public void setCodeResponse(Integer codeResponse) {
        this.codeResponse = codeResponse;
    }

    public String getMessResponse() {
        return messResponse;
    }

    public void setMessResponse(String messResponse) {
        this.messResponse = messResponse;
    }
}
