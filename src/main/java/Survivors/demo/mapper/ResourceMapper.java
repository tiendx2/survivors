package Survivors.demo.mapper;

import Survivors.demo.domain.Resource;
import Survivors.demo.dto.resource.ResourceDTO;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface ResourceMapper extends EntityMapper<ResourceDTO, Resource> {
    ResourceDTO toDto(Resource resource);

    Resource toEntity(ResourceDTO resourceDTO);

    default Resource fromId(Long id) {
        if (id == null) {
            return null;
        }

        Resource resource = new Resource();
        resource.setId(id);

        return resource;
    }
}
