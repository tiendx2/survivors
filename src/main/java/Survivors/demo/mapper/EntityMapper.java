package Survivors.demo.mapper;

import java.util.List;

/**
 * Contract for a generic Survivors.demo.dto to entity Survivors.demo.mapper.
 *
 * @param <D> - DTO type parameter.
 * @param <E> - Entity type parameter.
 */

public interface EntityMapper <D, E> {

    E toEntity(D dto);

    D toDto(E entity);

    List <E> toEntity(List<D> dtoList);

    List <D> toDto(List<E> entityList);
}
