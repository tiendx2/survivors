package Survivors.demo.mapper;

import Survivors.demo.domain.Survivor;
import Survivors.demo.dto.survivor.SurvivorDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {ResourceMapper.class})
public interface SurvivorMapper extends EntityMapper<SurvivorDTO, Survivor> {

    @Mapping(source = "resources", target = "resources")
    SurvivorDTO toDto(Survivor revDoubleCount);

    @Mapping(source = "resources", target = "resources")
    Survivor toEntity(SurvivorDTO survivorDTO);

    default Survivor fromId(Long id) {
        if (id == null) {
            return null;
        }
        Survivor survivor = new Survivor();
        survivor.setId(id);
        return survivor;
    }
}
