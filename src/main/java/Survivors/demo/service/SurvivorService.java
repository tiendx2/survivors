package Survivors.demo.service;

import Survivors.demo.domain.Survivor;
import Survivors.demo.dto.survivor.SurvivorDTO;
import Survivors.demo.dto.survivor.SurvivorValidatedDTO;

public interface SurvivorService {
    boolean INFECTED = true;
    boolean NON_INFECTED = false;

    SurvivorDTO createData(Survivor survivor);

    SurvivorValidatedDTO validateUpdateLastLocation(Long survivorId, Double latitude, Double longitude);

    Survivor updateLastLocation(Survivor survivor, Double latitude, Double longitude);

    SurvivorValidatedDTO validateCreateData(SurvivorDTO survivorDTO);

    SurvivorValidatedDTO validateUpdateInfected(Long survivorId);

    Survivor updateInfected(Survivor survivor);

    Float getPercentageInfected(Boolean isInfected);
}
