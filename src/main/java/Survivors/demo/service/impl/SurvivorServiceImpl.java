package Survivors.demo.service.impl;

import Survivors.demo.config.Constants;
import Survivors.demo.domain.Resource;
import Survivors.demo.domain.Survivor;
import Survivors.demo.dto.survivor.SurvivorDTO;
import Survivors.demo.dto.survivor.SurvivorValidatedDTO;
import Survivors.demo.mapper.SurvivorMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import Survivors.demo.repository.SurvivorRepository;
import Survivors.demo.service.SurvivorService;

import java.time.Instant;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class SurvivorServiceImpl implements SurvivorService {
    private SurvivorRepository survivorRepository;

    private SurvivorMapper survivorMapper;

    @Autowired
    public SurvivorServiceImpl(SurvivorRepository survivorRepository, SurvivorMapper survivorMapper) {
        this.survivorRepository = survivorRepository;
        this.survivorMapper = survivorMapper;
    }

    @Override
    public SurvivorDTO createData(Survivor survivor) {
        survivor.setInfected(NON_INFECTED);
        Survivor survivorSaved = this.survivorRepository.save(survivor);

        return this.survivorMapper.toDto(survivorSaved);
    }

    @Override
    public Survivor updateLastLocation(Survivor survivor, Double latitude, Double longitude) {
        survivor.setLastLocationLatitude(latitude);
        survivor.setLastLocationLongitude(longitude);
        survivor.setModifiedOn(Instant.now());

        return this.survivorRepository.save(survivor);
    }

    @Override
    public Survivor updateInfected(Survivor survivor) {
        survivor.setInfected(INFECTED);
        survivor.setModifiedOn(Instant.now());

        return this.survivorRepository.save(survivor);
    }

    @Override
    public Float getPercentageInfected(Boolean isInfected) {
        Float percentage = null;
        List<Survivor> survivors = this.survivorRepository.findAll();

        if (survivors.size() > 0) {
            List<Survivor> survivorsFilters =
                    survivors.stream().filter(survivor -> survivor.isInfected() == isInfected).collect(Collectors.toList());
            percentage = survivorsFilters.size() * 100f / survivors.size();
        }

        return percentage;
    }

    @Override
    public SurvivorValidatedDTO validateUpdateLastLocation(Long survivorId, Double latitude, Double longitude) {
        int codeResponse = Constants.SUCCESS;
        String messResponse = "";
        Survivor survivor = null;

        if (survivorId == null || latitude == null || longitude == null) {
            codeResponse = Constants.ERROR;
            messResponse = "Invalid parameter";
        }

        if (codeResponse == Constants.SUCCESS) {
            SurvivorValidatedDTO survivorValidated = this.validateExistSurvivor(survivorId);
            codeResponse = survivorValidated.getCodeResponse();
            messResponse = survivorValidated.getMessResponse();
            survivor = survivorValidated.getSurvivor();
        }

        return new SurvivorValidatedDTO(survivor, codeResponse, messResponse);
    }

    @Override
    public SurvivorValidatedDTO validateCreateData(SurvivorDTO survivorDTO) {
        int codeResponse = Constants.SUCCESS;
        String messResponse = "";
        Survivor survivor = null;

        if (survivorDTO.getId() != null) {
            codeResponse = Constants.ERROR;
            messResponse = "Invalid parameter";
        }

        if (codeResponse == Constants.SUCCESS) {
            survivor = this.convertSurvivorDTO(survivorDTO);
        }

        return new SurvivorValidatedDTO(survivor, codeResponse, messResponse);
    }

    @Override
    public SurvivorValidatedDTO validateUpdateInfected(Long survivorId) {
        int codeResponse = Constants.SUCCESS;
        String messResponse = "";
        Survivor survivor = null;

        if (survivorId == null) {
            codeResponse = Constants.ERROR;
            messResponse = "Invalid parameter";
        }

        if (codeResponse == Constants.SUCCESS) {
            SurvivorValidatedDTO survivorValidated = this.validateExistSurvivor(survivorId);
            codeResponse = survivorValidated.getCodeResponse();
            messResponse = survivorValidated.getMessResponse();
            survivor = survivorValidated.getSurvivor();
        }

        return new SurvivorValidatedDTO(survivor, codeResponse, messResponse);
    }

    private SurvivorValidatedDTO validateExistSurvivor(Long survivorId) {
        int codeResponse = Constants.SUCCESS;
        String messResponse = "";
        Survivor survivor;

        survivor = this.survivorRepository.findFirstById(survivorId);

        if (survivor == null) {
            codeResponse = Constants.ERROR;
            messResponse = "Survivor is not existed";
        } else {
            if (survivor.isInfected()) {
                codeResponse = Constants.ERROR;
                messResponse = "Survivor is Infected";
            }
        }

        return new SurvivorValidatedDTO(survivor, codeResponse, messResponse);
    }

    private Survivor convertSurvivorDTO(SurvivorDTO SurvivorDTO) {
        Survivor survivor = this.survivorMapper.toEntity(SurvivorDTO);
        survivor.setInfected(false);

        List<Resource> resources = survivor.getResources();

        if (resources != null && resources.size() > 0) {
            for (Resource resource : resources) {
                resource.setCreatedOn(Instant.now());
                resource.setModifiedOn(Instant.now());
                resource.setSurvivor(survivor);
            }
        }

        return survivor;
    }
}
