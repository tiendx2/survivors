package Survivors.demo.response;

/**
 * A abstract class ApiResponse.
 */
public class ApiForCustomResponse<T> {

    private int code;

    private T data;

    private String error_code;

    private String error_message;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public String getError_code() {
        return error_code;
    }

    public void setError_code(String error_code) {
        this.error_code = error_code;
    }

    public String getError_message() {
        return error_message;
    }

    public void setError_message(String error_message) {
        this.error_message = error_message;
    }

    public ApiForCustomResponse(int code, T data, String error_code, String error_message) {
        super();
        this.code = code;
        this.data = data;
        this.error_code = error_code;
        this.error_message = error_message;
    }

}
