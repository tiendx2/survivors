package Survivors.demo.repository;

import Survivors.demo.domain.Survivor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
public interface SurvivorRepository extends JpaRepository <Survivor, Long> {

    Survivor findFirstById(Long survivorId);
}
