package Survivors.demo.web;

import Survivors.demo.config.Constants;
import Survivors.demo.domain.Survivor;
import Survivors.demo.dto.survivor.SurvivorDTO;
import Survivors.demo.dto.survivor.SurvivorValidatedDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import Survivors.demo.response.ApiForCustomResponse;
import Survivors.demo.service.SurvivorService;

@RestController
@RequestMapping("/api/survivor")
public class SurvivorResource {

    private final Logger log = LoggerFactory.getLogger(SurvivorResource.class);

    private SurvivorService survivorService;

    @Autowired
    public SurvivorResource(SurvivorService survivorService) {
        this.survivorService = survivorService;
    }


    @PostMapping("/create-data")
    public ResponseEntity<?> createData(@RequestBody SurvivorDTO SurvivorDTO) {
        log.debug("REST request to create a Survivor");

        int codeResponse;
        String messResponse;
        SurvivorDTO survivorSaved = null;

        SurvivorValidatedDTO survivorValidated = this.survivorService.validateCreateData(SurvivorDTO);
        codeResponse = survivorValidated.getCodeResponse();
        messResponse = survivorValidated.getMessResponse();

        if (survivorValidated.getCodeResponse() == Constants.SUCCESS) {
            survivorSaved = this.survivorService.createData(survivorValidated.getSurvivor());

            if (survivorSaved == null) {
                codeResponse = Constants.ERROR;
                messResponse = "Save data error";
            } else {
                messResponse = "Save data successfully";
            }
        }

        return ResponseEntity.ok().body(new ApiForCustomResponse<>(codeResponse, survivorSaved, "", messResponse));
    }

    @PutMapping("/update-Last-location")
    public ResponseEntity<?> updateLastLocation(
            @RequestParam(value = "survivorId") Long survivorId,
            @RequestParam(value = "latitude") Double latitude,
            @RequestParam(value = "longitude") Double longitude) {
        log.debug("REST request to update last location");

        int codeResponse;
        String messResponse;
        Survivor survivorSaved;

        SurvivorValidatedDTO survivorValidated =
                this.survivorService.validateUpdateLastLocation(survivorId, latitude, longitude);

        codeResponse = survivorValidated.getCodeResponse();
        messResponse = survivorValidated.getMessResponse();
        if (codeResponse == Constants.SUCCESS) {
            survivorSaved = this.survivorService.updateLastLocation(survivorValidated.getSurvivor(), latitude, longitude);

            if (survivorSaved == null) {
                codeResponse = Constants.ERROR;
                messResponse = "Save data error";
            } else {
                messResponse = "Save data successfully";
            }
        }

        return ResponseEntity.ok().body(new ApiForCustomResponse<>(codeResponse, "", "", messResponse));
    }

    @PutMapping("/update-infected")
    public ResponseEntity<?> updateInfected(
            @RequestParam(value = "survivorId") Long survivorId) {
        log.debug("REST request to update Infected to true");

        int codeResponse;
        String messResponse;

        SurvivorValidatedDTO survivorValidated =
                this.survivorService.validateUpdateInfected(survivorId);

        codeResponse = survivorValidated.getCodeResponse();
        messResponse = survivorValidated.getMessResponse();
        if (survivorValidated.getCodeResponse() == Constants.SUCCESS) {
            Survivor survivorSaved = this.survivorService.updateInfected(survivorValidated.getSurvivor());

            if (survivorSaved == null) {
                codeResponse = Constants.ERROR;
                messResponse = "Save data error";
            } else {
                messResponse = "Save data successfully";
            }
        }


        return ResponseEntity.ok().body(new ApiForCustomResponse<>(codeResponse, "", "", messResponse));
    }

    @GetMapping("/get-percentage-infected")
    public ResponseEntity<?> getPercentageInfected(
            @RequestParam(value = "isInfected") Boolean isInfected) {
        log.debug("REST request to get percentage infected");

        Float percentage = this.survivorService.getPercentageInfected(isInfected);

        return ResponseEntity.ok().body(new ApiForCustomResponse<>(Constants.SUCCESS, percentage, "", ""));
    }
}
